function getdata() {
    fetch(`https://restcountries.com/v3.1/all`)
        .then(data => data.json())
        .then(fdata => display(fdata))
        .catch((err)=>{
            console.log(err);
        })
        
}
var div_main = document.getElementById("main");


function display(fdata)
 {
        fdata.forEach(element => {
   
    console.log(element);
    
        const G=(Object.keys(element.currencies))
    
    
    div_main.innerHTML+=`
    <div class="row bd mar">
            <div class="col-4">
                <div>
                    <div>
                        <img src=" ${element.flags.png} " class="img" alt="...">
                    </div>
                </div>
            </div>
            <div class="col">
                <div>
                    <div class="pad">
                        
                            <h4 id="cntryName">${element.name.common}</h4><b>
                                <h6>Currency: ${element.currencies[G].name} </h6>
                                <h6>Current date and time: ${calcTime(element.timezones)}</h6>
                                <a href="${element.maps.googleMaps}" target="_blank"><button class="btn1"><b>SHOW MAPS</b></button></a>
                                <a href="detail.html?Country_ID=${element.cca3}" target="_blank"><button class="btn1"><b>DETAILS</b></button></a>
                    </div>
                </div>
            </div>`
 });
    
 }
 function calcTime(off) {

    const ans = off[0].substring(3,6);

    const d = new Date();

   const localTime = d.getTime();

   const localOffset = d.getTimezoneOffset() * 60000;

   const utc = localTime + localOffset;

   const offset = ans;

   const dubai = utc + (3600000 * offset);

   const date = new Date(dubai).toLocaleString();

 return date;

 }


 
let search=document.getElementById("searchbox");
 let searchbtn=document.getElementById("searchbtn");
 searchbtn.addEventListener('click', searchfn);
//  console.log(searchbtn);
 function searchfn(){
    div_main.innerHTML = ""
    if(search.value == ""){

        fetch(`https://restcountries.com/v3.1/all`)
        .then(data => data.json())
        .then(fdata => display(fdata))

    }
    else{
  fetch(`https://restcountries.com/v3.1/name/${search.value}`)
    .then(res => res.json())
    .then(data => display(data))
    }

 }





//  ${element.currencies.map((cur) => cur.name)}


 